package controller;

import java.io.Serializable;


import nutsAndBolts.PieceSquareColor;

/**
 * @author francoise.perrin
 * 
 * Objet cr�� par le Model dans m�thode MoveCapturePromote()
 * � destination du Controller qui en extrait les donn�es pour cr�er
 * l'objet InputViewModel � destination de la View
 * 
 */
public class OutputModelData<T> implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	public boolean isMoveDone = false;
	public T capturedPieceCoord = null;
	public T promotedPieceCoord = null;
	public PieceSquareColor promotedPieceColor = null;
	public int whitePieces = 0;
	public int blackPieces = 0;
	public String CurrentPlayer = null;
	public int whiteWins = 0;
	public int blackWins = 0;
	public String gameStatus = null;
	
	public OutputModelData(
			boolean isMoveDone, 
			T capturedPieceCoord,
			T promotedPieceCoord,
			PieceSquareColor promotedPieceColor,
			int whitePieces,
			int blackPieces,
			String CurrentPlayer,
			int whiteWins,
			int blackWins,
			String gameStatus) {
		super();
		this.isMoveDone = isMoveDone;
		this.capturedPieceCoord = capturedPieceCoord;
		this.promotedPieceCoord = promotedPieceCoord;
		this.promotedPieceColor = promotedPieceColor;
		this.whitePieces = whitePieces;
		this.blackPieces = blackPieces;
		this.CurrentPlayer = CurrentPlayer;
		this.whiteWins = whiteWins;
		this.blackWins = blackWins;
		this.gameStatus = gameStatus;
	}


	@Override
	public String toString() {
		return "DataAfterMove [isMoveDone=" + isMoveDone + ", capturedPieceIndex=" + capturedPieceCoord
				+ ", promotedPieceIndex=" + promotedPieceCoord + ", promotedPieceColor=" + promotedPieceColor + "]";
	}


	

	
}
