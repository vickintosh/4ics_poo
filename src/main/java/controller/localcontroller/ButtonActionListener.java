package controller.localcontroller;

public interface ButtonActionListener {
    void onPlayAgain();
    void onEndSeries();
    void onExitApp();
}
