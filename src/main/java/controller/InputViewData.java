package controller;

import java.io.Serializable;



import nutsAndBolts.PieceSquareColor;

/**
 * @author francoise.perrin
 *
 * Objet � destination de la View
 * cr�� par le Controller
 * � partir des donn�es retourn�es par le Model
 * 
 */
public class InputViewData<T> implements Serializable{
	
	private static final long serialVersionUID = 1L;

	public T toMovePieceIndex = null;
	public T targetSquareIndex = null;
	public T capturedPieceIndex = null;
	public T promotedPieceIndex = null;
	public PieceSquareColor promotedPieceColor = null;
	public int whitePieces = 0;
	public int blackPieces = 0;
	public String CurrentPlayer = null;
	public int whiteWins = 0;
	public int blackWins = 0;
	public String gameStatus = null;
	
	
	public InputViewData(
			T toMovePieceIndex, 
			T targetSquareIndex, 
			T capturedPieceIndex,
			T promotedPieceIndex,
			PieceSquareColor promotedPieceColor,
			int whitePieces,
			int blackPieces,
			String CurrentPlayer,
			int whiteWins,
			int blackWins,
			String gameStatus) {
		super();
		this.toMovePieceIndex = toMovePieceIndex;
		this.targetSquareIndex = targetSquareIndex;
		this.capturedPieceIndex = capturedPieceIndex;
		this.promotedPieceIndex = promotedPieceIndex;
		this.promotedPieceColor = promotedPieceColor;
		this.whitePieces = whitePieces;
		this.blackPieces = blackPieces;
		this.CurrentPlayer = CurrentPlayer;
		this.whiteWins = whiteWins;
		this.blackWins = blackWins;
		this.gameStatus = gameStatus;
	}

	@Override
	public String toString() {
		return "DataAfterMove [toMovePieceIndex=" + toMovePieceIndex
				+ ", targetSquareIndex=" + targetSquareIndex + ", capturedPieceIndex=" + capturedPieceIndex
				+ ", promotedPieceIndex=" + promotedPieceIndex + ", promotedPieceColor=" + promotedPieceColor + "]";
	}


	

	
}
