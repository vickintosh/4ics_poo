package model;

import nutsAndBolts.PieceSquareColor;


import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


/**
 * @author francoise.perrin
 * 
 * Cete classe fabrique et stocke toutes les PieceModel du Model dans une collection 
 * elle est donc responsable de rechercher et mettre � jour les PieceModel (leur position)
 * 
 * En revanche, elle n'est pas responsable des algorithme m�tiers li�s au d�placement des pi�ces
 * (responsabilit� de la classe Model)
 */
public class ModelImplementor {

	// la collection de pi�ces en jeu - m�lange noires et blanches
	private Collection<PieceModel> pieces ;

	// Variables pour stocker le nombre initial de pièces et le nombre de pièces prises
	private int initialWhitePieces;
	private int initialBlackPieces;
	private int whitePiecesTaken = 0;
	private int blackPiecesTaken = 0;
	private String currentPlayerColor;
	private int whiteWins = 0;
	private int blackWins = 0;


	public ModelImplementor() {
		super();

		pieces = ModelFactory.createPieceModelCollection();

		// Initialisation du nombre de pièces pour chaque couleur
		initialWhitePieces = countPiecesByColor(PieceSquareColor.WHITE);
		initialBlackPieces = countPiecesByColor(PieceSquareColor.BLACK);

		this.currentPlayerColor = "WHITE";
	}

	public PieceSquareColor getPieceColor(Coord coord) {
		PieceSquareColor color = null;
		PieceModel piece = this.findPiece(coord);

		if (piece != null) {
			color = piece.getPieceColor();
		}
		return color;
	}
	
	
	public void setColorInCaseOfRafle(String color) {
		this.currentPlayerColor = color;
	}

	public boolean isPiecehere(Coord coord) {
		return this.findPiece(coord) != null;
	}

	public boolean isMovePieceOk(Coord initCoord, Coord targetCoord, boolean isPieceToTake) {

		boolean isMovePieceOk = false;
		PieceModel initPiece = this.findPiece(initCoord);
		if (initPiece != null) {
			isMovePieceOk = initPiece.isMoveOk(targetCoord, isPieceToTake ) ;
		}
		return isMovePieceOk;
	}


	public boolean movePiece(Coord initCoord, Coord targetCoord) {

		boolean isMovePieceDone = false;
		PieceModel initPiece = this.findPiece(initCoord);
		if (initPiece != null) {

			// d�placement pi�ce
			initPiece.move(targetCoord) ;
			isMovePieceDone = true;

			// Si le déplacement a été effectué avec succès, le tour passe à l'autre joueur
			if(isMovePieceDone) {
				this.currentPlayerColor = (this.currentPlayerColor.equals("WHITE")) ? "BLACK" : "WHITE";
			}
		}
		return isMovePieceDone;
	}

    public void removePiece(Coord pieceToTakeCoord) {
        PieceModel pieceToTake = this.findPiece(pieceToTakeCoord);
        if (pieceToTake != null) {
            
            // Mise à jour du nombre de pièces prises
            if (pieceToTake.getPieceColor() == PieceSquareColor.WHITE) {
                blackPiecesTaken++;
            } else {
                whitePiecesTaken++;
            }
            
            this.pieces.remove(pieceToTake);
        }
    }

	public boolean isPiecePromotable(Coord coord) {
		boolean isPiecePromotable = false;

		PieceModel piece = this.findPiece(coord);
		if (piece != null && piece instanceof Promotable) {
			Promotable toPromotePiece = (Promotable) piece;
			isPiecePromotable = toPromotePiece.isPromotable(); 
		}
		return isPiecePromotable;
	}

	public void promotePiece(Coord coord) {

		PieceModel piece = this.findPiece(coord);
		if (piece != null) {
			PieceSquareColor color = piece.getPieceColor();
			this.pieces.remove(piece);
			this.pieces.add(new QueenModel(coord, color)); 
		}
	}

	public List<Coord> getCoordsOnItinerary(Coord initCoord, Coord targetCoord) {
		List<Coord> coordsOnItinerary = null;
		PieceModel initPiece = this.findPiece(initCoord);
		if (initPiece != null) {
			coordsOnItinerary = initPiece.getCoordsOnItinerary(targetCoord) ;
		}

		return coordsOnItinerary;
	}

	public List<Coord> getTargetCoordsInMultiJumpCase(Coord pieceToMoveAgainCoord) {
		List<Coord> listPossibleTargetWithTake = null;
		PieceModel pieceToMoveAgain = this.findPiece(pieceToMoveAgainCoord);
		if (pieceToMoveAgain != null) {
			listPossibleTargetWithTake = pieceToMoveAgain.getTargetCoordsInMultiJumpCase();
		}
		return listPossibleTargetWithTake;
	}

	/**
	 * @param coord
	 * @return la pi�ce qui se trouve aux coordonn�es indiqu�es
	 */
	private PieceModel findPiece(Coord coord) {		
		PieceModel findPiece = null;

		for(PieceModel piece : this.pieces) {

			if(coord != null && piece.hasThisCoord(coord)) {
				findPiece = piece;
				break;
			}
		}
		return findPiece;
	}

	public int countPiecesByColor(PieceSquareColor color) {
		int count = 0;
		for(PieceModel piece : this.pieces) {
			if (piece.getPieceColor() == color) {
				count++;
			}
		}
		return count;
	}

	public String getCurrentPlayerColor() {
		return this.currentPlayerColor.substring(0, 1) + this.currentPlayerColor.substring(1).toLowerCase();
	}



	public String getWinner() {
		if (whitePiecesTaken == initialBlackPieces) {
			return "White";
		} else if (blackPiecesTaken == initialWhitePieces) {
			return "Black";
		} else {
			return null;
		}
	}

	public void setWins () {
		String winner = getWinner();
		if (winner == "White") {
			whiteWins++;
		} else if (winner == "Black") {
			blackWins++;
		}
	}

	//getter for whitewins
	public int getWhiteWins() {
		return whiteWins;
	}

	//getter for blackwins
	public int getBlackWins() {
		return blackWins;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 * 
	 * La m�thode toString() retourne une repr�sentation 
	 * de la liste de pi�ces sous forme d'un tableau 2D
	 * 
	 */
	//	public String toString() {
	//
	//
	//		String st = "";
	//		String[][] damier = new String[ModelConfig.LENGTH][ModelConfig.LENGTH];
	//
	//		// cr�ation d'un tableau 2D avec les noms des pi�ces � partir de la liste de pi�ces
	//		for(PieceModel piece : this.pieces) {
	//
	//			PieceSquareColor color = piece.getPieceColor();
	//			String stColor = (PieceSquareColor.WHITE.equals(color) ? "--B--" : "--N--" );
	//
	//			int col = piece.getColonne() -'a';
	//			int lig = piece.getLigne() -1;
	//			damier[lig][col ] = stColor ;
	//		}
	//
	//		// Affichage du tableau formatt�
	//		st = "     a      b      c      d      e      f      g      h      i      j\n";
	//		for ( int lig = 9; lig >=0 ; lig--) {
	//			st += (lig+1) + "  ";
	//			for ( int col = 0; col <= 9; col++) {					 
	//				String stColor = damier[lig][col];				
	//				if (stColor != null) {						
	//					st += stColor + "  ";	
	//				} 
	//				else {
	//					st += "-----  ";
	//				}
	//			}
	//			st +="\n";
	//		}
	//		return "Damier du model \n" + st;	
	//	}

	public String toString() {

//		Collections.sort((List<PieceModel>) pieces); // --> OK
//		Collections.sort((List<PieceModel>)pieces, new PieceComparator());  // --> OK
//		Collections.sort((Set<PieceModel>) pieces); // --> KO compilation : pas tri sur Set
//
//		Collections.sort((List<PieceModel>)pieces, new Comparator<PieceModel>()
//					{
//						public int compare(PieceModel o1, PieceModel o2) {
//			
//							int comp = 0;
//							int max = ModelConfig.LENGTH;
//			
//							// Colonne croissant, ligne croissant
//							comp =  (o1.getColonne()*max - o2.getColonne()*max) 
//									- (o2.getLigne() - o1.getLigne());
//			
//							return comp;
//						}
//					}
//				);  // --> OK

		String st = "";
		String nb = "";
		int i = 1;
		String winner = getWinner();
		setWins();

		// Parcours collection en utilisant un it�rateur

		//		for(ListIterator<PieceModel> it = ((List<PieceModel>)pieces).listIterator(); it.hasNext(); i++) {
		for(Iterator<PieceModel> it = pieces.iterator(); it.hasNext(); i++) {
			PieceModel piece = it.next();
			st += piece;
			if (i%5 == 0)
				st+= "\n";
		}

		// Ajout du nombre de pièces pour chaque couleur
		int whitePieces = countPiecesByColor(PieceSquareColor.WHITE);
		int blackPieces = countPiecesByColor(PieceSquareColor.BLACK);
        nb += "\nWhite pieces left: " + whitePieces + "/" + initialWhitePieces + "\n";
        nb += "Black pieces left: " + blackPieces + "/" + initialBlackPieces + "\n";
        nb += "White pieces taken: " + whitePiecesTaken + "/" + initialWhitePieces + "\n";
        nb += "Black pieces taken: " + blackPiecesTaken + "/" + initialWhitePieces + "\n";
		if (winner == null) {
			nb += "Game in progress\n";
			nb += "Current player: " + getCurrentPlayerColor() + "\n";
		} else {
			nb += "Game over. " + winner + " won!\n";
		}
		nb += "White wins = " + whiteWins + "\n";
		nb += "Black wins = " + blackWins + "\n";

		return "Damier du model \n" + st + nb;	
	}

	public void reset() {
		// Recréation de la collection de pièces
		this.pieces = ModelFactory.createPieceModelCollection();
	
		// Réinitialisation du nombre de pièces pour chaque couleur
		this.initialWhitePieces = countPiecesByColor(PieceSquareColor.WHITE);
		this.initialBlackPieces = countPiecesByColor(PieceSquareColor.BLACK);
	
		// Réinitialisation du nombre de pièces prises à 0 pour les deux couleurs
		this.whitePiecesTaken = 0;
		this.blackPiecesTaken = 0;
		
		// Mettre la couleur du joueur actuel à "WHITE"
		this.currentPlayerColor = "WHITE";

		// Regarde qui a gagé la partie précédente et incrémente le compteur de victoires
	}

	public void endGame() {
        Timer timer = new Timer();

        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                // Ceci est exécuté après le délai.
                System.exit(0);
            }
        };

        long delay = 20 * 1000; // 20 secondes
        timer.schedule(task, delay);
    }
	

}


