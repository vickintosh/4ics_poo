package gui;

import controller.InputViewData;
import controller.localcontroller.ButtonActionListener;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class UIBuilder {

	private VBox seriesLabelBox;
	private VBox scoreLabelBox;
	private HBox buttonBox;
	private VBox TopBox;
	private VBox Midbox;
	private VBox BottomBox;



    private Label seriesTitle;
    private Label blackVictoryLabel;
    private Label whiteVictoryLabel;
    private Label currentPlayerLabel;
	private Label seriesRecap;
    private Label scoreTitle;
    private Label whitePiecesLabel;
    private Label blackPiecesLabel;

	private Button playAgainButton;
    private Button endSeriesButton;
	private Button exitAppButton;

	private ButtonActionListener actionListener;

	private int whiteVictory = 0;
	private int blackVictory = 0;

    public void setButtonActionListener(ButtonActionListener listener) {
        this.actionListener = listener;
    }


    public BorderPane buildRightPane() {
        // ... code pour construire le panneau de droite (Atelier 4)...

		// Création du titre pour la série
		seriesTitle = new Label("Series");
		seriesTitle.setStyle("-fx-font: 24 arial;");

		// Création des étiquettes pour la série de victoires
		blackVictoryLabel = new Label("Black victory : 0");
		blackVictoryLabel.setStyle("-fx-font: 18 arial;");
		whiteVictoryLabel = new Label("White victory : 0");
		whiteVictoryLabel.setStyle("-fx-font: 18 arial;");

		// Création d'un VBox pour contenir les labels de la série de victoires
		seriesLabelBox = new VBox();
		seriesLabelBox.setSpacing(10);
		seriesLabelBox.setAlignment(Pos.TOP_LEFT);  // Alignement des labels à gauche
		seriesLabelBox.getChildren().addAll(blackVictoryLabel, whiteVictoryLabel);

		// Création d'un VBox pour contenir le titre de la série et les labels
		TopBox = new VBox();
		TopBox.setSpacing(10);
		TopBox.setAlignment(Pos.CENTER);  // Centrage du titre de la série
		TopBox.getChildren().addAll(seriesTitle, seriesLabelBox);

		// Ajout des marges autour de TopBox
		BorderPane.setMargin(TopBox, new Insets(20));

		// Création du label pour le joueur actuel
		currentPlayerLabel = new Label("White to play");
		currentPlayerLabel.setStyle("-fx-font: 24 arial; -fx-text-fill: black;");

		// Création du label pour le score final
		seriesRecap = new Label("0 - 0");
		seriesRecap.setStyle("-fx-font: 18 arial; -fx-text-fill: black;");
		seriesRecap.setVisible(false);


		// Création des boutons pour recommencer et terminer la série
		playAgainButton = new Button("Play again");
		endSeriesButton = new Button("End series");
		exitAppButton = new Button("Exit app");

		// Les boutons sont initialement cachés
		playAgainButton.setVisible(false);
		endSeriesButton.setVisible(false);
		exitAppButton.setVisible(false);

        playAgainButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (actionListener != null) {
                    actionListener.onPlayAgain();
                }
            }
        });

        endSeriesButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (actionListener != null) {
                    actionListener.onEndSeries();
                }
            }
        });

		exitAppButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if (actionListener != null) {
					actionListener.onExitApp();
				}
			}
		});

		// Ajout des boutons à Midbox
		buttonBox = new HBox();
		buttonBox.setSpacing(10);
		buttonBox.setAlignment(Pos.CENTER);  // Centrage des boutons
		buttonBox.getChildren().addAll(playAgainButton, endSeriesButton);

		// Création d'un VBox pour contenir le label du joueur actuel
		Midbox = new VBox();
		Midbox.setSpacing(10);
		Midbox.setAlignment(Pos.CENTER);  // Centrage du label du joueur actuel
		Midbox.getChildren().add(currentPlayerLabel);
		Midbox.getChildren().add(buttonBox);
		Midbox.getChildren().add(seriesRecap);
		Midbox.getChildren().add(exitAppButton);

		// Ajout des marges autour de Midbox
		BorderPane.setMargin(Midbox, new Insets(20));

		// Création du titre pour les scores
		scoreTitle = new Label("Score");
		scoreTitle.setStyle("-fx-font: 24 arial;");

		// Création des étiquettes pour les scores
		whitePiecesLabel = new Label("White pawn : 20");
		whitePiecesLabel.setStyle("-fx-font: 18 arial;");
		blackPiecesLabel = new Label("Black pawn : 20");
		blackPiecesLabel.setStyle("-fx-font: 18 arial;");

		// Création d'un VBox pour contenir les labels des scores
		scoreLabelBox = new VBox();
		scoreLabelBox.setSpacing(10);
		scoreLabelBox.setAlignment(Pos.TOP_LEFT);  // Alignement des labels à gauche
		scoreLabelBox.getChildren().addAll(whitePiecesLabel, blackPiecesLabel);

		// Création d'un VBox pour contenir le titre des scores et les labels
		BottomBox = new VBox();
		BottomBox.setSpacing(10);
		BottomBox.setAlignment(Pos.CENTER);  // Centrage du titre des scores
		BottomBox.getChildren().addAll(scoreTitle, scoreLabelBox);

		// Ajout des marges autour de BottomBox
		BorderPane.setMargin(BottomBox, new Insets(20));

		// Ajout de TopBox et BottomBox à RightPane
		BorderPane RightPane = new BorderPane();
		RightPane.setTop(TopBox);
		RightPane.setCenter(Midbox);
		RightPane.setBottom(BottomBox);

        return RightPane;
    }

	public void refreshRightPane(InputViewData<Integer> dataToRefreshView) {
		// ... code pour mettre à jour le panneau de droite ...
		whitePiecesLabel.setText("White pawn : " + dataToRefreshView.whitePieces);
		blackPiecesLabel.setText("Black pawn : " + dataToRefreshView.blackPieces);
		currentPlayerLabel.setText(dataToRefreshView.CurrentPlayer + " to play");
		whiteVictoryLabel.setText("White victory : " + dataToRefreshView.whiteWins);
		blackVictoryLabel.setText("Black victory : " + dataToRefreshView.blackWins);
		whiteVictory = dataToRefreshView.whiteWins;
		blackVictory = dataToRefreshView.blackWins;

		if (dataToRefreshView.gameStatus == "White") {
			playAgainButton.setVisible(true);
			endSeriesButton.setVisible(true);
			currentPlayerLabel.setText("White wins");
		}
		else if (dataToRefreshView.gameStatus == "Black") {
			playAgainButton.setVisible(true);
			endSeriesButton.setVisible(true);
			currentPlayerLabel.setText("Black wins");
		}

	}

	public void reset() {
		// ... code pour réinitialiser le panneau de droite ...
		whitePiecesLabel.setText("White pawn : 20");
		blackPiecesLabel.setText("Black pawn : 20");
		currentPlayerLabel.setText("White to play");
		playAgainButton.setVisible(false);
		endSeriesButton.setVisible(false);
	}

	public void endGame() {
		// ... code pour afficher le gagnant de la série et quitter le jeu ... (affiche le texte sur 2 lignes)
		if (whiteVictory > blackVictory) {
			currentPlayerLabel.setText("White wins\nthe series !");
		} else if (blackVictory > whiteVictory) {
			currentPlayerLabel.setText("Black wins\nthe series !");
		} else {
			currentPlayerLabel.setText("It's a tie!");
		}

		//supprime la Hbox contenant les boutons
		playAgainButton.setVisible(false);
		endSeriesButton.setVisible(false);
		Midbox.getChildren().remove(buttonBox);

		//affiche le score final
		seriesRecap.setText(whiteVictory + " - " + blackVictory);
		seriesRecap.setVisible(true);
		exitAppButton.setVisible(true);
	}
}